/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QDebug>
#include <QScreen>
#include <QWindow>
#include <QPainter>
#include <QFileInfo>
#include <QResizeEvent>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QShortcut>
#include <QMessageBox>
#include <QProcess>

#include "pageactivites.h"
#include "apps/pageapps.h"
#include "pagehome.h"
#include "pagepins.h"
#include "pagesessions.h"
#include "pagetasks.h"
#include "slidingstackedwidget.h"
#include "corestuff.h"
#include "ui_corestuff.h"
#include "powerdlg.h"

#include <cprime/cprime.h>


corestuff::corestuff(QWidget *parent): QWidget(parent)
  , ui( new Ui::corestuff )
  , smi(new settings)
  , gesture(new GestureHandler(this))
{
	/* Enforcing theme set in qt5ct */
	if (QFileInfo(QDir::home().filePath(".config/qt5ct/qt5ct.conf")).exists()) {
		QSettings qt5ct( "qt5ct", "qt5ct" );
		QIcon::setThemeName( qt5ct.value( "Appearance/icon_theme" ).toString() );
	}

    ui->setupUi(this);

    QPalette pltt(palette());
    pltt.setColor(QPalette::Base, Qt::transparent);
    setPalette(pltt);

    loadSettings();
    startSetup();
    setupIcons();
    shortcuts();

    pHome = new pagehome(this);
    pApps = new pageapps(this);
    pTasks = new pagetasks(this);

    connect(pTasks, SIGNAL(activateDock()), this, SIGNAL(activateDock()));

    ui->pHomeLayout->addWidget(pHome);
    ui->pAppsLayout->addWidget(pApps);
    ui->pTasksLayout->addWidget(pTasks);

    // Set coreapps page as active page
    on_home_clicked();

    ui->menu->setVisible(0);
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
    ui->clearActivites->setVisible(0);

    qApp->installEventFilter(this);

    setMouseTracking( true );
}

corestuff::~corestuff()
{
	delete smi;
    delete ui;
}

void corestuff::closeEvent(QCloseEvent *event)
{
    for (int i = 0; i < ui->pages->count(); i++) {
        if (ui->pages->widget(i)->children().count() == 2)
            if (ui->pages->widget(i)->children().at(1)) {
                ui->pages->widget(i)->children().at(1)->deleteLater();
            }
    }

    event->accept();
}

/**
 * @brief Loads application settings
 */
void corestuff::loadSettings()
{
    showActivities = smi->getValue("CoreApps", "KeepActivities");
    toolsIconSize = smi->getValue("CoreApps", "ToolsIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    wallpaperPos = smi->getValue("CoreStuff", "WallpaperPositon");
}

void corestuff::startSetup()
{
    // corestuff as desktop
    setGeometry(qApp->primaryScreen()->availableGeometry());
    setFixedSize(qApp->primaryScreen()->availableSize());
    setAttribute(Qt::WA_X11NetWmWindowTypeDesktop);
    setWindowFlags(Qt::FramelessWindowHint);

    // icon only pagebuttons
    for (QToolButton *b : ui->pageButtons->findChildren<QToolButton *>()) {
        b->setToolButtonStyle(Qt::ToolButtonIconOnly);
        b->setIconSize(toolsIconSize);
    }

    ui->navBtnGroup->setId(ui->home, 0);
    ui->navBtnGroup->setId(ui->apps, 1);
    ui->navBtnGroup->setId(ui->tasks, 2);
    ui->navBtnGroup->setId(ui->pins, 3);

    if (!showActivities) {
        ui->activites->setVisible(false);
        ui->navBtnGroup->removeButton(ui->activites);
        ui->navBtnGroup->setId(ui->sessions, 4);
    } else {
        ui->navBtnGroup->setId(ui->activites, 4);
        ui->navBtnGroup->setId(ui->sessions, 5);
    }

    fswStart = new QFileSystemWatcher(this);
    connect(fswStart, &QFileSystemWatcher::fileChanged, [ = ](const QString & path) {
        if (QFileInfo::exists(path)) {
            fswStart->addPaths(QStringList() << path);
        }
        qDebug() << "Watching... " << fswStart->files();
        reload(path);
    });

    fswStart->addPaths(QStringList() << CPrime::Variables::CC_ActivitiesFilePath()
                       << CPrime::Variables::CC_PinsFilePath()
                       /*<< CPrime::Variables::CC_CoreApps_SessionFilePath()*/);

    resizeWindow();

    updateBGPix();
}

void corestuff::setupIcons(){
    ui->apps->setIcon(CPrime::ThemeFunc::themeIcon( "view-grid-symbolic", "view-grid", "view-grid"));
    ui->tasks->setIcon(CPrime::ThemeFunc::themeIcon( "view-refresh-symbolic", "view-refresh", "view-refresh" ));
    ui->pins->setIcon(CPrime::ThemeFunc::themeIcon( "bookmark-new-symbolic", "bookmark-new-symbolic", "bookmark-new" ));
    ui->activites->setIcon(CPrime::ThemeFunc::themeIcon( "preferences-system-time-symbolic", "preferences-system-time", "preferences-system-time" ));
    ui->sessions->setIcon(CPrime::ThemeFunc::themeIcon( "weather-snow-symbolic", "weather-snow", "weather-snow" ));
    ui->home->setIcon(CPrime::ThemeFunc::themeIcon( "user-home-symbolic", "user-home", "user-home" ));
    ui->editSessoion->setIcon(CPrime::ThemeFunc::themeIcon( "view-refresh-symbolic", "view-refresh", "view-refresh" ));
    ui->addSession->setIcon(CPrime::ThemeFunc::themeIcon( "document-new-symbolic", "document-new", "document-new" ));
    ui->clearActivites->setIcon(CPrime::ThemeFunc::themeIcon( "user-trash-symbolic", "user-trash", "user-trash" ));
    ui->menu->setIcon(CPrime::ThemeFunc::themeIcon( "open-menu-symbolic", "application-menu", "open-menu" ));
    ui->deleteSession->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
    ui->power->setIcon(CPrime::ThemeFunc::themeIcon( "system-log-out-symbolic", "system-log-out", "system-log-out" ));
}

void corestuff::shortcuts()
{
    QShortcut *shortcut;
    shortcut = new QShortcut(QKeySequence(Qt::Key_Left), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::previousPage);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Right), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::nextPage);

    shortcut = new QShortcut(QKeySequence(Qt::Key_1), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_home_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_2), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_apps_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_3), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_tasks_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_4), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_pins_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_5), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_activites_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_6), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_sessions_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::CTRL | Qt::ALT | Qt::Key_Delete), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_power_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_PowerOff), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_power_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_VolumeUp), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::VolumeUp);

    shortcut = new QShortcut(QKeySequence(Qt::Key_VolumeDown), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::VolumeDown);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Print), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::TakeCoreshot);

    shortcut = new QShortcut(QKeySequence(Qt::Key_SysReq), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::TakeCoreshot);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Meta), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_tasks_clicked);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Meta + Qt::Key_D), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::showDesktopT);

    shortcut = new QShortcut(QKeySequence(Qt::Key_Menu), this);
    connect(shortcut, &QShortcut::activated, this, &corestuff::on_menu_clicked);
}

void corestuff::updateBGPix()
{
	QString pixPath = smi->getValue("CoreStuff", "Background");
    if (!QFileInfo(pixPath).exists()) {
        m_bgpix = QPixmap();
        return;
    }
    QPixmap pix(pixPath);

    QSize drawSize = qApp->primaryScreen()->geometry().size();

    pixOffsetX = 0;
    pixOffsetY = 0;

    switch (wallpaperPos) {
        // Centered: Show the full wallpaper at the center
        case 0: {
            pix = pix.scaled( drawSize, Qt::KeepAspectRatio, Qt::SmoothTransformation );
            pixOffsetX = abs(pix.width() - drawSize.width()) / 2;
            pixOffsetY = abs(pix.height() - drawSize.height()) / 2;

            break;
        }

        // Stretched: Ignore aspect ratio
        case 1: {
            pix = pix.scaled( drawSize, Qt::IgnoreAspectRatio, Qt::SmoothTransformation );
            break;
        }

        // Scaled: Qt::KeepAspectRatioByExpanding
        case 2: {
            pix = pix.scaled( drawSize, Qt::KeepAspectRatioByExpanding, Qt::SmoothTransformation );
            break;
        }
    }

    m_bgpix = pix;
}

void corestuff::resizeWindow()
{
    // Detect screen rotation and take necessary steps
    QScreen *scrn = qApp->primaryScreen();//qApp->screens()[screenIndex];

    connect(scrn, &QScreen::availableGeometryChanged, [this, scrn](const QRect & geometry) {
        setGeometry(geometry);
        qDebug() << "Geometry Changed to: " << geometry;

        setFixedSize(geometry.size());
        qDebug() << "Screen size Changed to: " << scrn->size();
    });
}

void corestuff::previousPage()
{
    int currId = ui->navBtnGroup->checkedId();
    if (currId <= 0) {
        return;
    }

    ui->navBtnGroup->button(currId - 1)->click();
}

void corestuff::nextPage()
{
    int currId = ui->navBtnGroup->checkedId();
    if (currId >= ui->navBtnGroup->buttons().count() - 1) {
        return;
    }

    ui->navBtnGroup->button(currId + 1)->click();
}

void corestuff::reload(const QString &path)
{
    QFileInfo fi(path);

    if (fi.fileName() == CPrime::Variables::CC_ActivitiesFilePath()) {
        if (showActivities) {
            if (pActivities) {
                pActivities->reload();
            }
        }
    } else if (fi.fileName() == CPrime::Variables::CC_PinsFilePath()) {
        if (pPins) {
            pPins->reload();
        }
    } else if (fi.fileName() == CPrime::Variables::CC_System_ConfigDir() + "coreapps/session") {
        if (pSessions) {
            pSessions->reload();
        }
    }
}

void corestuff::pageClick(QToolButton *btn, int i, QString windowTitle)
{
    // all button checked false
    for (QToolButton *b : ui->pageButtons->findChildren<QToolButton *>()) {
        b->setChecked(false);
    }

    btn->setChecked(true);
    slidingStackedWidget::SlidingDirection direction;
    if (i < ui->pages->currentIndex()) {
        direction = slidingStackedWidget::RIGHTTOLEFT;
    } else {
        direction = slidingStackedWidget::LEFTTORIGHT;
    }
    ui->pages->slideInto(i, direction);
    currentPage = i;
    setWindowTitle(windowTitle + " - CoreStuff");

    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
    ui->clearActivites->setVisible(0);

}

void corestuff::on_home_clicked()
{
    pageClick(ui->home, 0, "Home");
    ui->menu->setVisible(0);
}

void corestuff::on_apps_clicked()
{
    pageClick(ui->apps, 1, "Apps");
    ui->menu->setVisible(0);
}

void corestuff::on_tasks_clicked()
{
    pTasks->updateClientList();
    pageClick(ui->tasks, 2, "Tasks");
    ui->menu->setVisible(0);
}

void corestuff::on_pins_clicked()
{
    if (ui->pages->widget(3)->children().count() < 2) {
        pPins = new pagepins(this);
        ui->pPinsLayout->addWidget(pPins);
    }

    pageClick(ui->pins, 3, "Pins");
    ui->menu->setVisible(0);
}

void corestuff::on_activites_clicked()
{
    if (ui->pages->widget(4)->children().count() < 2) {
        pActivities = new pageactivites(this);
        QList<QToolButton *> list;
        list.append(ui->menu);
        list.append(ui->clearActivites);
        pActivities->addControls(list);
        ui->pActivitesLayout->addWidget(pActivities);

        // Configure Recent Activity
        if (showActivities) {
            QString raFile = CPrime::Variables::CC_ActivitiesFilePath();
            QFile file(raFile);

            if (!file.exists()) {
                // You can get error
                // Need a check here
                file.open(QIODevice::ReadWrite | QIODevice::Text);
                file.close();
            }
        }
    }

    pageClick(ui->activites, 4, "Activites");
    ui->addSession->setVisible(0);
    ui->deleteSession->setVisible(0);
    ui->editSessoion->setVisible(0);
    ui->menu->setVisible(0);
    ui->clearActivites->setVisible(1);
}

void corestuff::on_sessions_clicked()
{
    if (ui->pages->widget(5)->children().count() < 2) {
        pSessions = new pagesessions(this);
        QList<QToolButton *> list;
        list.append(ui->menu);
        list.append(ui->addSession);
        list.append(ui->editSessoion);
        list.append(ui->deleteSession);
        pSessions->addControls(list);
        ui->pSessionsLayout->addWidget(pSessions);
    }

    pageClick(ui->sessions, 5, "Sessions");
    ui->menu->setVisible(1);
    ui->clearActivites->setVisible(0);
}

void corestuff::on_clearActivites_clicked()
{
    // link to pageactivites.cpp
    pActivities->clearActivities();
}

void corestuff::on_editSessoion_clicked()
{
    // link to pagesession.cpp
    pSessions->editSessoion();
}

void corestuff::on_deleteSession_clicked()
{
    // link to pagesession.cpp
    pSessions->deleteSession();
}

void corestuff::on_addSession_clicked()
{
    // link to pagesession.cpp
    pSessions->addSession();
}

void corestuff::on_menu_clicked()
{
    if (!ui->addSession->isVisible()) {   // session
        ui->addSession->setVisible(1);
        ui->deleteSession->setVisible(1);
        ui->editSessoion->setVisible(1);
        ui->clearActivites->setVisible(0);
    } else{
        ui->addSession->setVisible(0);
        ui->deleteSession->setVisible(0);
        ui->editSessoion->setVisible(0);
        ui->clearActivites->setVisible(0);
    }
}

void corestuff::handleMessages(const QString msg)
{
    // This is basically to reload the background
    if (msg == "reload") {
        repaint();
    }
}

void corestuff::paintEvent(QPaintEvent *pEvent)
{
    QPainter painter(this);
    if (m_bgpix.isNull()) {
        QWidget::paintEvent(pEvent);
        return;
    }

    painter.drawPixmap(pixOffsetX, pixOffsetY, m_bgpix.width(), m_bgpix.height(), m_bgpix);

    QWidget::paintEvent(pEvent);
    pEvent->accept();
}

void corestuff::showTasksPage()
{
    on_tasks_clicked();
}

void corestuff::showDesktopT()
{
    pTasks->showDesktop();
}

void corestuff::VolumeUp()
{
    QProcess proc;
    proc.startDetached("pactl", QStringList() << "--" << "set-sink-volume" << "0" << "+5%");
}

void corestuff::VolumeDown()
{
    QProcess proc;
    proc.startDetached("pactl", QStringList() << "--" << "set-sink-volume" << "0" << "-5%");
}

void corestuff::TakeCoreshot()
{
    QProcess proc;
    proc.startDetached("coreshot", QStringList() << "");
}

void corestuff::on_power_clicked()
{
    PowerDialog *pwrDlg = new PowerDialog(this);
    pwrDlg->exec();
}

void corestuff::mousePressEvent(QMouseEvent *event)
{
    if(event->button() == Qt::LeftButton)
    {
        qDebug () << "LeftButton";
//        if(gestures)
            if(ui->pages->geometry().contains(event->pos())) // mouse is in the mpvFrame
                gesture->Begin(event->globalPosition().toPoint());


    }
    else if(event->button() == Qt::RightButton )
    {
        qDebug () << "RightButton";
    }
    QWidget::mousePressEvent(event);
}

void corestuff::mouseReleaseEvent(QMouseEvent *event)
{
    gesture->End();
    QWidget::mouseReleaseEvent(event);
}

void corestuff::mouseMoveEvent(QMouseEvent *event)
{
    if(gesture->Process(event->globalPosition().toPoint()))
        event->accept();

    QWidget::mouseMoveEvent(event);
}

void corestuff::leaveEvent(QEvent *event)
{
    qDebug () << "leaveEvent";
    // mouseMoveEvent(new QMouseEvent(QMouseEvent::MouseMove,
    //                                QCursor::pos(),
    //                                Qt::NoButton,Qt::NoButton,Qt::NoModifier));
    QWidget::leaveEvent(event);
}

bool corestuff::eventFilter(QObject *obj, QEvent *event)
{
    if( event->type() == QEvent::MouseMove)
    {
        QMouseEvent *mouseEvent = static_cast<QMouseEvent*>(event);
        mouseMoveEvent(mouseEvent);
    }
    else if(event->type() == 6) // QEvent::KeyPress = 6  (but using QEvent::KeyPress causes compile errors, not sure why)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent*>(event);
        keyPressEvent(keyEvent);
    }
    return false;
}
