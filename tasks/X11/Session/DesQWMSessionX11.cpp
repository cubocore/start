/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "DesQWMSession.hpp"
#include "DesQWMSessionX11.hpp"

#include <qpa/qplatformnativeinterface.h>

static inline int getAppScreen() {
    // Get the X11 display
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (!display) {
        qWarning("Unable to retrieve X11 display.");
        return -1;
    }

    // Get the default screen number
    return DefaultScreen(display);
}

static inline Window getAppRootWindow() {
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (!display) {
        qWarning("Unable to retrieve X11 display.");
        return 0;
    }

    return DefaultRootWindow(display);
}

static inline unsigned long getAppUserTime() {
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (display == nullptr) {
        return 0;
    }

    int time = 0;
    XGetInputFocus(display, None, &time);

    return time;
}

DesQWMSessionPrivate::DesQWMSessionPrivate( DesQWMSession *ds ) {

	q = ds;

	mConn = qApp->nativeInterface<QNativeInterface::QX11Application>()->connection();

	if ( not mConn ) {
		mEWMHValid = false;
		return;
	}

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( mConn, &EWMH );
    mEWMHValid = ( bool )xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr );
};

DesQWMSession *DesQWMSession::mCurSession = nullptr;

DesQWMSession* DesQWMSession::currentSession() {

	if ( not mCurSession )
		mCurSession = new DesQWMSession();

	return mCurSession;
};

DesQWMSession::DesQWMSession() : QObject() {

	d = new DesQWMSessionPrivate( this );
};

Window DesQWMSession::getActiveWindow() {

	if ( not d->mEWMHValid )
		return 0;

	xcb_window_t window = 0;

	xcb_get_property_cookie_t activeCookie = xcb_ewmh_get_active_window( &d->EWMH, getAppScreen() );
    xcb_ewmh_get_active_window_reply( &d->EWMH, activeCookie, &window, nullptr );

	return window;
};

void DesQWMSession::setActiveWindow( Window mWid ) {

	if ( not d->mEWMHValid )
		return;

    xcb_client_message_event_t event;

	event.response_type = XCB_CLIENT_MESSAGE;
	event.format = 32;
	event.sequence = 0;
	event.window = mWid;
	event.type = GetAtom( "_NET_ACTIVE_WINDOW" );
	event.data.data32[0] = 2L;
	event.data.data32[1] = getAppUserTime();

    xcb_send_event( d->mConn, 0, getAppRootWindow(), XCB_EVENT_MASK_SUBSTRUCTURE_NOTIFY | XCB_EVENT_MASK_SUBSTRUCTURE_REDIRECT, (const char *)&event );
	xcb_flush( d->mConn );
};

Windows DesQWMSession::listClients() {

	if ( not d->mEWMHValid )
		return Windows();

	xcb_ewmh_get_windows_reply_t windows;

	xcb_get_property_cookie_t listCookie = xcb_ewmh_get_client_list( &d->EWMH, getAppScreen() );
    int reply = xcb_ewmh_get_windows_reply( &d->EWMH, listCookie, &windows, nullptr );

	if ( not reply )
		return Windows();

	Windows winList;

	for( uint i = 0; i < windows.windows_len; i++ )
		winList << windows.windows[ i ];

	return winList;
};

void DesQWMSession::showDesktop() {

	if ( not d->mEWMHValid )
		return;

    uint32_t desktopShown = 0;
    int state = 0;

    xcb_get_property_cookie_t showCookie = xcb_ewmh_get_showing_desktop( &d->EWMH, getAppScreen() );
    xcb_ewmh_get_showing_desktop_reply( &d->EWMH, showCookie, &desktopShown, nullptr );

	/* If the desktop is being shown, undo it */
    if ( desktopShown ) {
        state = 0;
	}

	/* If the desktop is not being shown, show it */
    else {
        state = 1;
	}

	xcb_ewmh_request_change_showing_desktop( &d->EWMH, getAppScreen(), state );
};

void DesQWMSession::showOnAllDesktops( Window winID ) {

	if ( not d->mEWMHValid )
		return;

	qWarning() << "This does not seem to work properly. Use at your own risk.";

	xcb_ewmh_request_change_wm_desktop( &d->EWMH, getAppScreen(), winID, (quint32)( -1 ), XCB_EWMH_CLIENT_SOURCE_TYPE_NORMAL );
	xcb_ewmh_set_wm_desktop( &d->EWMH, winID, (quint32)( -1 ) );
	xcb_flush( d->mConn );
};

bool DesQWMSession::isCompositorRunning() {

	if ( not d->mEWMHValid )
		return false;

	xcb_get_selection_owner_cookie_t ownerCookie = xcb_ewmh_get_wm_cm_owner_unchecked( &d->EWMH, getAppScreen() );

	xcb_window_t owner;
	if ( xcb_ewmh_get_wm_cm_owner_reply( &d->EWMH, ownerCookie, &owner, nullptr ) ) {

		if ( owner )
			return true;
	}

	return false;
};

QSize DesQWMSession::desktopSize() {

	if ( not d->mEWMHValid )
		return QGuiApplication::primaryScreen()->size();

    xcb_screen_t          *screen = nullptr;
    int                    screen_nbr;
    xcb_screen_iterator_t  iter;

    screen_nbr = xcb_setup_roots_length( xcb_get_setup( d->mConn ) );

    /* Get the screen #screen_nbr */
    iter = xcb_setup_roots_iterator( xcb_get_setup( d->mConn ) );
    for( ; iter.rem; --screen_nbr, xcb_screen_next( &iter ) ) {
        if ( screen_nbr == 0 ) {
            screen = iter.data;
            break;
        }
    }

	if ( screen )
    	return QSize( screen->width_in_pixels, screen->height_in_pixels );

	return QSize();
};
