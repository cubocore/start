/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#pragma once

#include <DesQGlobals.hpp>

class DesQWMSessionPrivate;

class DesQWMSession : public QObject {
	Q_OBJECT

	public:
		/* Current session instance */
		static DesQWMSession *currentSession();

		/* Window ID of the active window */
		Window getActiveWindow();

		/* Window ID of the active window */
		void setActiveWindow( Window );

		/* List all the clients */
		Windows listClients();

		/* Show the desktop */
		void showDesktop();

		/* Show @winID on all desktops */
		void showOnAllDesktops( Window winID );

		/* Check if the compositor is running */
		bool isCompositorRunning();

		/* Size of the desktop */
		QSize desktopSize();

	private:
		DesQWMSession();
		static DesQWMSession *mCurSession;

	protected:
		DesQWMSessionPrivate *d;

	Q_SIGNALS:
		void resizeDesktop();
		void listWindows();
		void activeWindowChanged();
};
