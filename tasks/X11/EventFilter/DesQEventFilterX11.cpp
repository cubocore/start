/*
	*
	* Copyright 2019 Britanicus <marcusbritanicus@gmail.com>
	*
	* This file is a part of DesQ project (https://gitlab.com/desq/)
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* ( at your option ) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/

#include "DesQGlobals.hpp"

#include "DesQWMSession.hpp"
#include "DesQClient.hpp"
#include "X11Helpers.hpp"

#include "DesQEventFilterX11.hpp"

#include <xcb/xcb_event.h>
#include <X11/Xlib.h>
#include <X11/extensions/XInput.h>
#include <X11/extensions/XInput2.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <stdlib.h>

#include <qpa/qplatformnativeinterface.h>

static inline Window getAppRootWindow() {
    Display* display = static_cast<Display*>(QGuiApplication::platformNativeInterface()->nativeResourceForIntegration("display"));
    if (!display) {
        qWarning("Unable to retrieve X11 display.");
        return 0;
    }

    return DefaultRootWindow(display);
}

DesQEventFilterPrivate::DesQEventFilterPrivate( DesQEventFilter *ef ) {

	q = ef;

	xcb_intern_atom_cookie_t *cookie = xcb_ewmh_init_atoms( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), &EWMH );
	mEWMHValid = ( bool )xcb_ewmh_init_atoms_replies( &EWMH, cookie, nullptr );

	WinNotifyAtoms << EWMH._NET_WM_NAME << EWMH._NET_WM_VISIBLE_NAME << EWMH._NET_WM_ICON_NAME;
	WinNotifyAtoms << EWMH._NET_WM_VISIBLE_ICON_NAME << EWMH._NET_WM_ICON << EWMH._NET_WM_ICON_GEOMETRY;

	SysNotifyAtoms << EWMH._NET_CLIENT_LIST << EWMH._NET_CLIENT_LIST_STACKING << EWMH._NET_CURRENT_DESKTOP;
	SysNotifyAtoms << EWMH._NET_WM_STATE << EWMH._NET_ACTIVE_WINDOW;
}

static void register_existing_windows(xcb_connection_t *conn, xcb_window_t root) {
	int i, len;
	xcb_window_t *children;
	xcb_query_tree_reply_t *reply;

	uint32_t mask = XCB_EVENT_MASK_FOCUS_CHANGE | XCB_EVENT_MASK_PROPERTY_CHANGE | XCB_EVENT_MASK_BUTTON_PRESS |
					XCB_EVENT_MASK_BUTTON_RELEASE | XCB_EVENT_MASK_POINTER_MOTION | XCB_EVENT_MASK_EXPOSURE;

	if ( ( reply = xcb_query_tree_reply( conn, xcb_query_tree( conn, root ), 0 ) ) == NULL )
		return;

	len = xcb_query_tree_children_length( reply );
	children = xcb_query_tree_children( reply );
	for (i = 0; i < len; i++) {
		xcb_change_window_attributes( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), children[ i ], XCB_CW_EVENT_MASK | XCB_CW_CURSOR, &mask );
		register_existing_windows( conn, children[ i ] );
	}

  	xcb_flush(conn);
};

DesQEventFilter::DesQEventFilter( DesQWMSession *sess ) : QAbstractNativeEventFilter(), mSession( sess ) {

	d = new DesQEventFilterPrivate( this );

	uint32_t mask = XCB_EVENT_MASK_FOCUS_CHANGE | XCB_EVENT_MASK_PROPERTY_CHANGE;
	xcb_change_window_attributes( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), getAppRootWindow(), XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK, &mask );

	xcb_screen_t *screen = xcb_setup_roots_iterator( xcb_get_setup( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection() ) ).data;
	xcb_change_window_attributes( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), screen->root, XCB_CW_EVENT_MASK, &mask );

	/* Watch all the events of the already open windows as well */
	Q_FOREACH( Window window, sess->listClients() )
		xcb_change_window_attributes( qApp->nativeInterface<QNativeInterface::QX11Application>()->connection(), window, XCB_CW_EVENT_MASK, &mask );
};

/* This code is based on Qt5 documentation */
bool DesQEventFilter::nativeEventFilter( const QByteArray &eventType, void *message, qintptr * ) {

	if ( eventType == "xcb_generic_event_t" ) {

		/* Convert the event to xcb_generic_event_t* so that we can pasre it */
		xcb_generic_event_t *ev = static_cast<xcb_generic_event_t *>( message );

		/* Parse the above event and emit necessary signals */
		switch( ev->response_type & ~0x80 ) {

			/* Some property has changed */
			case XCB_PROPERTY_NOTIFY: {

				xcb_property_notify_event_t *pev = ( xcb_property_notify_event_t* )ev;

				if ( pev->window == getAppRootWindow() ) {
					if ( pev->atom == d->EWMH._NET_CURRENT_DESKTOP ) {
						// This means current desktop has changed.
						// qDebug() << "Current desktop changed";
					}

					else if ( ( pev->atom == d->EWMH._NET_DESKTOP_GEOMETRY ) or ( pev->atom == d->EWMH._NET_WORKAREA ) ) {
						// The desktop geometry has changed
						// Might be caused due to switch to external screen?
						// qDebug() << "Desktop geometry/workarea changed";
						mSession->resizeDesktop();
					}

					else if ( d->SysNotifyAtoms.contains( pev->atom ) ) {
						// Property of some window has changed, active window
						// stacking order, icon, etcxcb_selection_clear_event_t
						// pev->window is the pointer to that window.

						if ( pev->atom == d->EWMH._NET_CLIENT_LIST ) {
							// qDebug() << "Window added or removed.";
							mSession->listWindows();
						}

						else if ( pev->atom == d->EWMH._NET_ACTIVE_WINDOW ) {
							// qDebug() << "Active window changed";
							mSession->activeWindowChanged();
						}
					}

					else if ( d->WinNotifyAtoms.contains( pev->atom ) ) {
						// Property of some window name or icon has changed
						// pev->window is the window id.
						// qDebug() << "Window changed (2):" << pev->window;
						// qDebug() << GetAtomName( pev->atom );
					}
				}

				else {
					// qDebug() << "Window changed (3):" << pev->window;
					// qDebug() << GetAtomName( pev->atom );
				}

				break;
			}

			/* Message from the client about some change */
			case XCB_CLIENT_MESSAGE: {

				// Check all windows in general for changes
				// qDebug() << "Window changed (4):" << ((xcb_client_message_event_t*)ev)->window;
				// qDebug() << ( ( xcb_client_message_event_t* )ev )->data;
				break;
			}

			case XCB_SELECTION_CLEAR: {
				// We do nothing here
				qDebug() << "What happened here?";
				break;
			}

			case XCB_MOTION_NOTIFY: {
				xcb_motion_notify_event_t *mev = (xcb_motion_notify_event_t *)ev;
				qDebug() << "Mouse moved in window" << mev->event << "at coordinates " << mev->event_x << mev->event_y;
				break;
			}

			case XCB_BUTTON_PRESS: {
				xcb_button_press_event_t *mev = (xcb_button_press_event_t *)ev;
				// print_modifiers(mev->state);

				switch (mev->detail) {
					case 4:
						printf ("Wheel Button up in window %d, at coordinates (%d,%d)\n",
						mev->event, mev->event_x, mev->event_y);
						break;
					case 5:
						printf ("Wheel Button down in window %d, at coordinates (%d,%d)\n",
						mev->event, mev->event_x, mev->event_y);
						break;
					default:
						printf ("Button %d pressed in window %d, at coordinates (%d,%d)\n",
						mev->detail, mev->event, mev->event_x, mev->event_y);
				}
				break;
			}

			case XCB_BUTTON_RELEASE: {
				xcb_button_release_event_t *mev = (xcb_button_release_event_t *)ev;
				// print_modifiers(ev->state);

				printf ("Button %d released in window %d, at coordinates (%d,%d)\n", mev->detail, mev->event, mev->event_x, mev->event_y);
				break;
		    }

			default:{
				// qDebug() << "Some event which we are not watching.";
				break;
			}
		}
	}

	else {

		qDebug() << eventType;
	}

	/* We do not want to filter the above events, just intercept them for our work */
	return false;
};

DesQMouseCatcher::DesQMouseCatcher() : QThread() {

	mStop = false;
	mCurrentCorner = -1;
};

void DesQMouseCatcher::stop() {

	mStop = true;
	mAllowEmit = true;
}

void DesQMouseCatcher::run() {

	Display *display = XOpenDisplay( NULL );
	XIEventMask mask[2];
	XIEventMask *m;
	Window win = DefaultRootWindow( display );
	int event, error;

	int xi_opcode;
	if ( !XQueryExtension( display, "XInputExtension", &xi_opcode, &event, &error ) ) {
		qDebug() << "XInputExtension unavailable...!";
		return;
	}

	/* Select for motion events */
	m = &mask[0];
	m->deviceid = XIAllDevices;
	m->mask_len = XIMaskLen( XI_LASTEVENT );
	m->mask = ( unsigned char* )calloc( m->mask_len, sizeof( unsigned char ) );
	XISetMask( m->mask, XI_RawKeyPress );
	XISetMask( m->mask, XI_KeyPress );
	XISetMask( m->mask, XI_ButtonPress );
	XISetMask( m->mask, XI_ButtonRelease );
	XISetMask( m->mask, XI_Motion );
	XISetMask( m->mask, XI_PropertyEvent );

	m = &mask[1];
	m->deviceid = XIAllMasterDevices;
	m->mask_len = XIMaskLen( XI_LASTEVENT );
	m->mask = ( unsigned char* )calloc( m->mask_len, sizeof( char ) );

	/* Get mouse events from root window */
	XISelectEvents( display, win, &mask[0], 2 );

	/* Get mouse events from X11 client windows as well */
	Q_FOREACH( Window win, DesQWMSession::currentSession()->listClients() )
		XISelectEvents( display, win, &mask[0], 2 );

	XSync( display, False );

	free( mask[0].mask );
	free( mask[1].mask );

	while( not mStop ) {

		XEvent ev;
		XGenericEventCookie *cookie = ( XGenericEventCookie* )&ev.xcookie;
		XNextEvent( display, ( XEvent* )&ev );

		if ( XGetEventData( display, cookie ) && cookie->type == GenericEvent && cookie->extension == xi_opcode ) {
			// printf( "EVENT type %d ( %s )\n", cookie->evtype, type_to_name( cookie->evtype ) );
			QPoint mPos = QCursor::pos();
			switch ( cookie->evtype ) {

				case XI_ButtonPress: {
					// qDebug() << "Pointer clicked...!" << QCursor::pos();
					emit mousePress( mPos );
					break;
				}

				case XI_ButtonRelease: {
					// qDebug() << "Pointer released...!";
					emit mouseRelease( mPos );
					break;
				}

				/*
					*
					* We're using this to detect hot corners.
					* Once the mouse goes to a hot corner, emitting of
					* hot-corner signals is disabled until it comes out.
					* Once it comes out, hot-corner deactivation signal
					* is emitted.
					*
				*/
				case XI_Motion: {
					emit mouseMove( mPos );
					QRect screen = qApp->primaryScreen()->geometry();

					QPoint topLeft, topRight, bottomLeft, bottomRight;

					topLeft = screen.topLeft();
					topRight = screen.topRight();
					bottomLeft = screen.bottomLeft();
					bottomRight = screen.bottomRight();

					QPoint topEdge, rightEdge, bottomEdge, leftEdge;

					topEdge    = QPoint( screen.width() / 2, screen.top() );
					rightEdge  = QPoint( screen.right(), screen.height() / 2 );
					bottomEdge = QPoint( screen.width() / 2, screen.bottom() );
					leftEdge   = QPoint( screen.left(), screen.height() / 2 );

					/* Top Left */
					if ( topLeft == mPos ) {
						if ( mAllowEmit ) {
							emit hotCornerActivated( Qt::TopLeftCorner );
							mCurrentCorner = Qt::TopLeftCorner;
							mAllowEmit = false;
						}
					}

					/* Bottom Left */
					else if ( bottomLeft == mPos ) {
						if ( mAllowEmit ) {
							emit hotCornerActivated( Qt::BottomLeftCorner );
							mCurrentCorner = Qt::BottomLeftCorner;
							mAllowEmit = false;
						}
					}

					/* Top Right */
					else if ( topRight == mPos ) {
						if ( mAllowEmit ) {
							emit hotCornerActivated( Qt::TopRightCorner );
							mCurrentCorner = Qt::TopRightCorner;
							mAllowEmit = false;
						}
					}

					/* Bottom Right */
					else if ( bottomRight == mPos ) {
						if ( mAllowEmit ) {
							emit hotCornerActivated( Qt::BottomRightCorner );
							mCurrentCorner = Qt::BottomRightCorner;
							mAllowEmit = false;
						}
					}

					// /* Top edge */
					// else if ( topEdge == mpos ) {
					// 	if ( mAllowEmit ) {
					// 		emit hotCornerActivated( Qt::TopEdge );
					// 		mCurrentCorner = Qt::TopLeftCorner;
					// 		mAllowEmit = false;
					// 	}
					// }
					//
					// /* Right edge */
					// else if ( bottomLeft == mpos ) {
					// 	if ( mAllowEmit ) {
					// 		emit hotCornerActivated( Qt::BottomLeftCorner );
					// 		mCurrentCorner = Qt::BottomLeftCorner;
					// 		mAllowEmit = false;
					// 	}
					// }
					//
					// /* Bottom edge */
					// else if ( topRight == mpos ) {
					// 	if ( mAllowEmit ) {
					// 		emit hotCornerActivated( Qt::TopRightCorner );
					// 		mCurrentCorner = Qt::TopRightCorner;
					// 		mAllowEmit = false;
					// 	}
					// }
					//
					// /* Left edge */
					// else if ( bottomRight == mpos ) {
					// 	if ( mAllowEmit ) {
					// 		emit hotCornerActivated( Qt::BottomRightCorner );
					// 		mCurrentCorner = Qt::BottomRightCorner;
					// 		mAllowEmit = false;
					// 	}
					// }

					else {
						emit hotCornerDeactivated( mCurrentCorner );
						mAllowEmit = true;
						mCurrentCorner = -1;
					}

					break;
				}

				default: {
					break;
				}
			}
		}

		XFreeEventData( display, cookie );
	}

	XDestroyWindow( display, win );
	XCloseDisplay( display );
};
