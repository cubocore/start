/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QLabel>
#include <QToolButton>
#include <QScroller>

#include "corestuff.h"
#include "pagetasks.h"
#include "ui_pagetasks.h"
#include "x11tasks.h"


pagetasks::pagetasks(QWidget *parent) : QWidget(parent)
  , ui(new Ui::pagetasks)
  , smi(new settings)
{
    ui->setupUi(this);

    loadSettings();
    startSetup();
}

pagetasks::~pagetasks()
{
	delete smi;
    delete ui;
}

/**
 * @brief Loads application settings
 */
void pagetasks::loadSettings()
{
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
}

void pagetasks::startSetup()
{
    ui->welcome->hide();
    ui->taskView->setIconSize(listViewIconSize);

    if ( uiMode != 0 ) {
        QScroller::grabGesture(ui->taskView, QScroller::LeftMouseButtonGesture);
    }

    mTasks = X11Tasks::session();
	mTasks->setBackendTarget( ui->taskView );
	mTasks->updateClientList();
	connect( mTasks, SIGNAL( resizeDesktop() ), this, SIGNAL( resizeDesktop() ) );
}

void pagetasks::updateClientList() {

	if ( mTasks )
		mTasks->updateClientList();
}

void pagetasks::showDesktop() {

    if ( mTasks )
		mTasks->showDesktop();

}
