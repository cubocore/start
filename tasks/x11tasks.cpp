/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include "x11tasks.h"

#include <QWidget>
#include <QToolButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QScroller>

#include <cprime/themefunc.h>

#include "settings.h"

TaskItem::TaskItem(QString title, QIcon icon, quint64 winID, QListWidgetItem *container, QWidget *parent) : QWidget(parent)
  , smi(new settings)
{
    m_title = title;
    m_icon = icon;
    m_winID = winID;
    m_container = container;
    QLabel *lblIcon = new QLabel();
    QLabel *lblTitle = new QLabel(title);
    QToolButton *btnClose = new QToolButton();
    btnClose->setToolButtonStyle(Qt::ToolButtonIconOnly);
    btnClose->setIcon(CPrime::ThemeFunc::themeIcon( "edit-delete-symbolic", "edit-delete", "edit-delete" ));
    btnClose->setIconSize(QSize(smi->getValue("CoreApps", "ListViewIconSize")));
    lblIcon->setPixmap(icon.pixmap((QSize)smi->getValue("CoreApps", "ListViewIconSize")));

    QHBoxLayout *lay = new QHBoxLayout;
    lay->addWidget(lblIcon);
    lay->addWidget(lblTitle);
    lay->addStretch();
    lay->addWidget(btnClose);


    setLayout(lay);

    connect(btnClose, &QToolButton::clicked, [this]() {
        emit closeOccured(m_container);
    });
}

QString TaskItem::getTitle() {

    return m_title;
}

QIcon TaskItem::getIcon() {

    return m_icon;
}

quint64 TaskItem::getWinID() {

    return m_winID;
}

QListWidgetItem *TaskItem::getContainer() {

    return m_container;
}

X11Tasks::~X11Tasks() {

    delete smi;
    delete mSession;
    delete target;
};

X11Tasks* X11Tasks::mTasks = nullptr;
bool X11Tasks::mInit = false;

X11Tasks* X11Tasks::session() {

	if ( not mInit )
		mTasks = new X11Tasks();

	return mTasks;
};

X11Tasks::X11Tasks() {

    if ( mInit )
        return;

    mSession = DesQWMSession::currentSession();
    smi = new settings();

    uiMode = smi->getValue("CoreApps", "UIMode");

    mInit = true;
};

void X11Tasks::setBackendTarget( QListWidget *tgt ) {

    /* If the target is not set, set it */
    if ( not target )
        target = tgt;

    connect( mSession, &DesQWMSession::listWindows, this, &X11Tasks::updateClientList );
    connect( mSession, &DesQWMSession::activeWindowChanged, this, &X11Tasks::updateActiveWindow );
    connect( target, &QListWidget::itemClicked, this, &X11Tasks::activateWindow );

    connect( mSession, SIGNAL( resizeDesktop() ), this, SIGNAL( resizeDesktop() ) );
};

void X11Tasks::activateWindow( QListWidgetItem *item ) {

    if ( item ) {
        Window winID = static_cast<unsigned long>(item->data( Qt::UserRole + 1 ).toLongLong());
        if ( winID > 0 ) {


            int uiMode = smi->getValue("CoreApps", "UIMode");

            if(uiMode != 0){
                // First minimize all windows
                Q_FOREACH( Window winID, mSession->listClients() ) {
                    DesQClient window( winID );

                    //if ( window.pid() == qApp->applicationPid() )
                    //    continue;

                    if ( window.type() == DesQClient::Normal ) {
                        window.minimize();
                    }
                }
            }

            // Focus the active window and maximize it
            DesQClient client( winID );
            client.activate();
            if( uiMode != 0 ) {
                if ( !client.isMaximized() ) {
                    client.maximize();
                }
            }
        }
    }
};

void X11Tasks::closeActiveApp(QListWidgetItem *container) {

    QListWidgetItem *item = container;//target->currentItem();
    if ( item ) {
        Window winID = static_cast<unsigned long>(item->data( Qt::UserRole + 1 ).toLongLong());
        if ( winID > 0 ) {
            DesQClient client( winID );
            client.close();

            target->itemWidget(item)->deleteLater();
            delete item;
            updateActiveWindow();
        }
    }
};

void X11Tasks::updateClientList() {

    target->clear();
    Window active = mSession->getActiveWindow();

    Windows newList;

    // Handle new clients.
    Q_FOREACH( Window winID, mSession->listClients() ) {
        DesQClient window( winID );

        if ( window.pid() == static_cast<qulong>(qApp->applicationPid()) )
            continue;

        if ( window.type() == DesQClient::Normal ) {
            newList << winID;

            /* New Window */
            if(uiMode != 0){
                if ( not oldList.contains( winID ) ) {
                    oldList << winID;

                    /* Maximize the new window */
                    if ( not window.isMaximized() )
                        window.maximize();
                }
            }

            QListWidgetItem *item = new QListWidgetItem();
            TaskItem *taskItm = new TaskItem(window.title(), window.icon(), static_cast<qulonglong>(winID), item, target);

            item->setSizeHint(QSize(taskItm->sizeHint()));
            item->setData( Qt::UserRole, static_cast<quint64>(window.pid()) );
            item->setData( Qt::UserRole + 1, static_cast<qulonglong>(winID));		// Store the winID. Using this, we can create a Client and perform actions on it.

            target->addItem( item );
            target->setItemWidget(item, taskItm);

            connect( taskItm, &TaskItem::closeOccured, this, &X11Tasks::closeActiveApp );

            /* Set the current item if it's active */
            if ( winID == active )
                target->setCurrentItem( item );
        }
    }

    /* Prune the closed windows */
    Q_FOREACH( Window id, oldList ) {
        if ( not newList.contains( id ) )
            oldList.removeAll( id );
    }
};

void X11Tasks::updateActiveWindow() {

    Window active = mSession->getActiveWindow();

    for( int i = 0; i < target->count(); i++ ) {
        QListWidgetItem *item = target->item( i );
        if ( item ) {
            Window winID = item->data( Qt::UserRole + 1 ).toULongLong();

            /* Set the current item */
            if ( ( winID > 0 ) and ( winID == active ) )
                target->setCurrentItem( item );
        }
    }
};

void X11Tasks::showDesktop() {

	/* Store the active app */
	Window activeApp = 0;

	/* Store the window states, 0: minimized, 1: normal/maximized */
	QMap<Window, int> windowStates;
	for( Window winID: mSession->listClients() ) {
		DesQClient window( winID );

		// CoreStuff will not figure in our calculations
		if ( window.pid() == static_cast<qulong>( qApp->applicationPid() ) )
			continue;

		// Ignore all docks, panels and desktops
		if ( window.type() != DesQClient::Normal )
			continue;

		windowStates[ winID ] = ( window.isMinimized() ? 0 : 1 );
		if ( window.isActiveWindow() )
			activeApp = winID;

		window.minimize();
	}

	/* If all are minimized and activeApp is 0, then we are "showing the desktop" */
	/* and we should restore the last known active window */

	/* Are all windows minimized? */
	QList<int> states = windowStates.values();
	int hasOpenWindows = std::accumulate( states.begin(), states.end(), 0 );

	/* We have open windows, save window states and exit */
	if ( hasOpenWindows ) {
		mActiveWinID = activeApp;
		mWindowStates = windowStates;
		return;
	}

	for( Window id: mWindowStates.keys() ) {
		/* Restore all others */
		if ( id != mActiveWinID ) {
			DesQClient window( id );
			window.restore();
		}
	}

	/* Restore the active at last; hope that it will be on top */
	if ( mActiveWinID != 0 ) {
		DesQClient active( mActiveWinID );
		active.restore();
		active.activate();
	}
}

QSize X11Tasks::desktopSize() {

    return mSession->desktopSize();
}
