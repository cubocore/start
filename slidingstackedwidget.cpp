/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

#include <QPropertyAnimation>
#include <QParallelAnimationGroup>

#include "slidingstackedwidget.h"


slidingStackedWidget::slidingStackedWidget(QWidget *parent)
    : QStackedWidget(parent),
      m_next(0),
      m_active(false),
      m_currPoint(0, 0)
{
}

slidingStackedWidget::~slidingStackedWidget()
{
}

void slidingStackedWidget::slideNext()
{
    int now = currentIndex();
    if (now < count() - 1)
        slideWidget(now + 1, widget(now + 1), LEFTTORIGHT);
}

void slidingStackedWidget::slidePrevious()
{
    int now = currentIndex();
    if (now > 0)
        slideWidget(now - 1, widget(now - 1), RIGHTTOLEFT);
}

void slidingStackedWidget::slideInto(int index, SlidingDirection direction)
{
    if (index < 0 || index >= count())
        return;

    slideWidget(index, widget(index), direction);
}

void slidingStackedWidget::slideWidget(int index, QWidget *wid, SlidingDirection direction)
{
    if (m_active)
        return;
    else
        m_active = true;

    if (currentIndex() == index) {
        m_active = false;
        return;
    }

    QWidget *nowWid = currentWidget();

    int offsetX = 0;
    int offsetY = 0;
    int widW = frameRect().width();
    int widH = frameRect().height();

    wid->setGeometry(offsetX, offsetY, widW, widH);

    if (direction == LEFTTORIGHT) {
        offsetX = -widW;
    } else if (direction == RIGHTTOLEFT) {
        offsetX = widW;
    }

    QPoint nextPoint = wid->pos();
    QPoint currPoint = nowWid->pos();

    m_currPoint = currPoint;

    wid->move(nextPoint.x() - offsetX, nextPoint.y() - offsetY);
    wid->show();
    wid->raise();

    QPropertyAnimation *animNow = new QPropertyAnimation(nowWid, "pos");
    animNow->setDuration(150);
    animNow->setEasingCurve(QEasingCurve::Linear);
    animNow->setStartValue(QPoint(currPoint.x(), currPoint.y()));
    animNow->setEndValue(QPoint(offsetX + currPoint.x(), offsetY + currPoint.y()));

    QPropertyAnimation *animNext = new QPropertyAnimation(wid, "pos");
    animNext->setDuration(150);
    animNext->setEasingCurve(QEasingCurve::Linear);
    animNext->setStartValue(QPoint(-offsetX + nextPoint.x(), offsetY + nextPoint.y()));
    animNext->setEndValue(QPoint(nextPoint.x(), nextPoint.y()));

    QParallelAnimationGroup *animGroup = new QParallelAnimationGroup(this);
    animGroup->addAnimation(animNow);
    animGroup->addAnimation(animNext);

    connect(animGroup, &QParallelAnimationGroup::finished, this, &slidingStackedWidget::animationDone);

    m_next = index;
    m_active = true;

    animGroup->start(QAbstractAnimation::DeleteWhenStopped);
}

void slidingStackedWidget::animationDone()
{
    int curr = currentIndex();
    setCurrentIndex(m_next);
    widget(curr)->hide();
    widget(curr)->move(m_currPoint);
    m_active = false;
    emit animationFinished();
}
