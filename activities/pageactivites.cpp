/*
    *
    * This file is a part of CoreStuff.
    * An activity viewer for C Suite.
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/

//#include <QFuture>
#include <QMessageBox>
#include <QFile>
#include <QToolButton>
//#include <QtConcurrent/QtConcurrent>
#include <QScroller>

#include <cprime/cprime.h>
#include <cprime/appopenfunc.h>

#include "pageactivites.h"
#include "ui_pageactivites.h"


pageactivites::pageactivites(QWidget *parent) : QWidget(parent)
  , ui(new Ui::pageactivites)
  , smi(new settings)
{
    ui->setupUi(this);

    qRegisterMetaType<QVector<int>>("QVector<int>");

    loadSettings();
    startSetup();
    setupActivitesView();


    connect(ui->activitesView, &QTreeWidget::itemClicked, this, &pageactivites::openSelectedActivites);
}

pageactivites::~pageactivites()
{
	delete smi;
    delete ui;
}

void pageactivites::reload()
{
    qDebug() << "Reloading activities...";
    setupActivitesView();
}

void pageactivites::addControls(QList<QToolButton *> controls)
{
    menu = controls[0];
    m_clearActivities = controls[1];
}

void pageactivites::clearActivities()
{
    clearActivites();
}

/**
 * @brief Loads application settings
 */
void pageactivites::loadSettings()
{
    listViewIconSize = smi->getValue("CoreApps", "ListViewIconSize");
    uiMode = smi->getValue("CoreApps", "UIMode");
    showActivities = smi->getValue("CoreApps", "KeepActivities");
}


void pageactivites::startSetup()
{
    ui->activitesView->setIconSize(listViewIconSize);

    if (uiMode != 0) {
        QScroller::grabGesture(ui->activitesView, QScroller::LeftMouseButtonGesture);
    }
}

void pageactivites::setupActivitesView()
{
    ui->activitesView->clear();

    if (!showActivities) {
        return;
    }

//    QFuture<void> f = QtConcurrent::run([this]() {

        QSettings recentActivity(CPrime::Variables::CC_ActivitiesFilePath(), QSettings::IniFormat);
        QStringList topLevel = recentActivity.childGroups();

        if (topLevel.count()) {
            topLevel = CPrime::SortFunc::sortDate(topLevel);
        }

        Q_FOREACH (QString group, topLevel) {
            QTreeWidgetItem *topTree = new QTreeWidgetItem();
            QString groupL = sentDateText(group);
            topTree->setText(0, groupL);
            topTree->setSizeHint(0, listViewIconSize);
            recentActivity.beginGroup(group);
            QStringList keys = recentActivity.childKeys();
            //CPrime::SortFunc::sortTime( keys );
            CPrime::SortFunc::sortTime(keys, Qt::DescendingOrder);

            Q_FOREACH (QString key, keys) {
                QTreeWidgetItem *child = new QTreeWidgetItem();
                QString value = recentActivity.value(key).toString();
                child->setText(0, value);
                child->setIcon(0, CPrime::ThemeFunc::getAppIcon(value.split("\t\t\t").at(0).toLower()));
                topTree->addChild(child);
            }

            recentActivity.endGroup();
            ui->activitesView->insertTopLevelItem(0, topTree);
        }
//    });

//    QFutureWatcher<void> *r = new QFutureWatcher<void>();
//    r->setFuture(f);
//    connect(r, &QFutureWatcher<void>::finished, [this]() {
        if (ui->activitesView->model()->hasIndex(0, 0)) {
            (ui->activitesView->setExpanded(ui->activitesView->model()->index(0, 0), true));
        }
//    });

    if (ui->activitesView->topLevelItemCount()) {
        ui->activitesView->setVisible(1);
        ui->welcome->setVisible(0);
    } else{
        ui->activitesView->setVisible(0);
        ui->welcome->setVisible(1);
    }

}

void pageactivites::clearActivites()
{
    QString msg = QString("Do you want to clear your activities?");
    QMessageBox message(QMessageBox::Question, tr("Confirmation"), msg, QMessageBox::Yes | QMessageBox::No);
    message.setWindowIcon(QIcon::fromTheme("cc.cubocore.CoreStuff"));

    int reply = message.exec();

    if (reply == QMessageBox::Cancel) {
        return;
    }

    if (reply == QMessageBox::Yes) {
        ui->activitesView->clear();
        QString filePath = CPrime::Variables::CC_ActivitiesFilePath();
        QFile(filePath).remove();

        QFile file(filePath);
        file.open(QIODevice::ReadWrite | QIODevice::Text);
        file.close();
    }
}

void pageactivites::openSelectedActivites(QTreeWidgetItem *item, int column)    // Open Recent activity on double click
{
    if (!item->text(column).contains("\t\t\t")) {
        return;
    }

    QStringList s = item->text(column).split("\t\t\t");

    QString appName = s.at(0);
    QString path = s.at(1);

    CPrime::AppOpenFunc::systemAppOpener(appName, QStringList(path));
}

void pageactivites::on_activitesView_itemSelectionChanged()
{
    if (ui->activitesView->currentItem()) {
        m_clearActivities->setChecked(menu->isChecked());
    } else {
        m_clearActivities->setChecked(0);
    }
}

QString pageactivites::sentDateText(const QString &dateTime)
{
    QDateTime given = QDateTime::fromString(dateTime, "dd.MM.yyyy");

    if (QDate::currentDate().toString("dd.MM.yyyy") == dateTime) {
        return QString("Today");
    } else {
        return QString(given.toString("MMMM dd"));
    }
}
